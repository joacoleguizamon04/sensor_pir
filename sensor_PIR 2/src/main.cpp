/*CODIGO SENSOR PIR*/

//Librerias 
#include <Arduino.h>

//Macros
#define PIR 2
#define LED 6

//Prototipos   
void Cambio (void);

//Variables 
volatile bool change = 0;

void setup() 
{
  pinMode (LED, OUTPUT);
  pinMode (PIR, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PIR), Cambio, CHANGE);
}

void loop()   
{
  if (change == 1) 
  {
    digitalWrite (LED, 1);

    delay (500); 
  }

  else 
  {
    digitalWrite (LED, 0);
  }

}

void Cambio() 
{
  change = !change;
}